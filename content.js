chrome.runtime.onMessage.addListener(function (msg, sender) {
    if (msg.findSeries) {
        getMatchingSeries();
    }
});

function getMatchingSeries() {
    let allMyOptions = document.querySelectorAll('div.trade--you select.series')[0].options;
    let allMySeries = [];
    let currentSeries = 0;
    while (currentSeries < allMyOptions.length) {
        allMySeries.push(allMyOptions[currentSeries++].innerHTML);
    }

    let allPartnerOptions = document.querySelectorAll('div.trade--partner select.series')[0].options;
    currentSeries = 0;
    let allPartnerSeries = [];
    while (currentSeries < allPartnerOptions.length) {
        allPartnerSeries.push(allPartnerOptions[currentSeries++].innerHTML);
    }

    let availableInMine = [];
    let availableInPartner = [];
    for (let i = 0; i < allMyOptions.length; i++) {
        for (let j = 0; j < allPartnerOptions.length; j++) {
            if (allMySeries[i] === allPartnerSeries[j]) {
                availableInMine.push(allMySeries[i]);
                availableInPartner.push(allPartnerSeries[j]);
                break;
            }
        }
    }
    let allMyOptionsLength = allMyOptions.length;
    for (let i=0, j=0; i < availableInMine.length; i++) {
        while (j < allMyOptionsLength) {
            if (availableInMine[i] == allMyOptions[j].innerHTML) {
                i++;j++;
            } else {
                allMyOptions.remove(j);
                allMyOptionsLength--;
            }
        }
    }
    let allPartnerOptionsLength = allPartnerOptions.length;
    for (let i=0, j=0; i < availableInPartner.length; i++) {
        while (j < allPartnerOptionsLength) {
            if (availableInPartner[i] == allPartnerOptions[j].innerHTML) {
                i++;j++;
            } else {
                allPartnerOptions.remove(j);
                allPartnerOptionsLength--;
            }
        }
    }
    document.querySelectorAll('button.trade--add--btn.you')[0].click();
    document.querySelectorAll('.duplicates input')[1].click();
    setTimeout(function(){ document.querySelectorAll('.unowned input')[1].click(); }, 2000);
}